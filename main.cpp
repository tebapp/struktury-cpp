#include <iostream>

using namespace std;

typedef unsigned int uint;

//typedef struct {
//    string nr_seryjny,nazwa;
//    string *makra; //<- TABLICA
//    bool led_red, led_green, led_blue, led_white;
//    string led_konfiguracja;
//    uint dpi;
//    int rodzaj_sensora;
//    string *przyciski;
//    int x,y;
//} Mysz;


struct Mysz{
    string nr_seryjny,nazwa;
    string *makra; //<- TABLICA
    bool led_red, led_green, led_blue, led_white;
    string led_konfiguracja;
    uint dpi;
    int rodzaj_sensora;
    string *przyciski;
    int x,y;
    bool porusz_kursorem(int kierunek){
        //0 - w lewo
        //1 - w prawo
        //2 - w górę
        //3 - w dół
        //4 - w lewą górę
        //5 - w prawą górę
        //6 - w lewą dół
        //7 - w prawą dół

        switch (kierunek) {
        //z case 0 trzeba by powielic na pozostałe przypadki
        case 0: (x>0) ? x-- : 0; break;
        case 1: x++; break;
        case 2: y--; break;
        case 3: y++; break;
        case 4: x--; y--; break;
        case 5: x++; y--; break;
        case 6: x--; y++; break;
        case 7: x++; y++; break;
        default:
            break;
        }
        return false;
    }
    int wcisnij_przycisk(int przycisk);
    int przekrec();
    bool wywolaj_makro(int przycisk);
    bool zmien_led();
    bool zmiana_dpi(int wartosc);
};



int main()
{
    int ilosc=0;
    cout << "Ile myszy chcesz utworzyc?: ";
    cin >> ilosc;
    Mysz nowaMysz[ilosc];
    nowaMysz[0].nr_seryjny="15121498";
    nowaMysz[0].nazwa="GENESISCX33";
    nowaMysz[0].makra= new string[3];
    nowaMysz[0].makra[0]="wstecz";
    nowaMysz[0].makra[1]="przod";
    nowaMysz[0].makra[2]="zmiana koloru LED";
    nowaMysz[0].led_red=nowaMysz[0].led_green=nowaMysz[0].led_blue=true;
    nowaMysz[0].led_white=false;
    nowaMysz[0].led_konfiguracja="aktywna_dioda";
    nowaMysz[0].dpi=400;
    nowaMysz[0].rodzaj_sensora=1;
    nowaMysz[0].przyciski=new string[7];
    nowaMysz[0].przyciski[0]="lewy";
    nowaMysz[0].przyciski[1]="prawy";
    nowaMysz[0].przyciski[2]="srodkowy";
    nowaMysz[0].przyciski[3]="dpi";
    nowaMysz[0].przyciski[4]="zmiana_led";
    nowaMysz[0].przyciski[5]="cofnij";
    nowaMysz[0].przyciski[6]="ponow";
    nowaMysz[0].x=nowaMysz[0].y=0;
    for (int i = 0; i < 200; i++) {
        nowaMysz[0].porusz_kursorem(3);
    }
    for (int i = 0; i < 50; i++) {
        nowaMysz[0].porusz_kursorem(5);
    }
    cout << "Wspolrzedne myszy: " << nowaMysz[0].x << ' '
         << nowaMysz[0].y << ' ' << nowaMysz[0].nazwa <<endl;
    return 0;
}


